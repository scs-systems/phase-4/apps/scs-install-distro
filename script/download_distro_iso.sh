#!/bin/bash

# Copy the ISO /var/www/html/iso
# Run this on the container
# eg: http://mirror.ox.ac.uk/sites/mirror.centos.org/7.7.1908/isos/x86_64/CentOS-7-x86_64-Minimal-1908.iso

RELEASE=CentOS-7-x86_64-Minimal-1908.iso
DOWNLOAD_URL="http://mirror.ox.ac.uk/sites/mirror.centos.org/7.7.1908/isos/x86_64/CentOS-7-x86_64-Minimal-1908.iso"
DOWNLOAD_DIR=/var/www/html/iso
mkdir -p "$DOWNLOAD_DIR"


curl -o "$DOWNLOAD_DIR/$RELEASE" $DOWNLOAD_URL
